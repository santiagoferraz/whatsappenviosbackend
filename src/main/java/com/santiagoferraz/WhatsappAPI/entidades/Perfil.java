package com.santiagoferraz.WhatsappAPI.entidades;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class Perfil {
    private String name;

    public String getName() { return name; }

    public void setName(String name) {
        this.name = name;
    }

    public Perfil getPerfil(WebDriver driver) throws InterruptedException {
        WebElement elementPerfil = new WebDriverWait(driver, 1).until(ExpectedConditions.elementToBeClickable(By.cssSelector("div[id='side'] div[style*='cursor: pointer;'] img[src^='https://web.whatsapp.com/pp']")));
        elementPerfil.click();
        WebElement elementName = new WebDriverWait(driver, 1).until(ExpectedConditions.elementToBeClickable(By.cssSelector("div[tabindex='-1'] div[class*='copyable-text selectable-text']")));
        this.name = elementName.getText();
        return this;
    }
}
