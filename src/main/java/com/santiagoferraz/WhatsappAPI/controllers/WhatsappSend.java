package com.santiagoferraz.WhatsappAPI.controllers;

import com.santiagoferraz.WhatsappAPI.entidades.Perfil;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

@RestController
public class WhatsappSend {

    @GetMapping(value="/send-message")
    public void getTeste() {
        System.setProperty("webdriver.chrome.driver", "C:\\selenium-webdriver\\chromedriver.exe");
        String userProfile= "./Google/Chrome/User Data/";
        ChromeOptions options = new ChromeOptions();
        options.addArguments("user-data-dir="+userProfile);
        options.addArguments("--start-maximized");
        options.addArguments("chrome.switches", "--disable-extensions") ;
        WebDriver driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        driver.get("https://web.whatsapp.com/");
        driver.navigate().to("https://api.whatsapp.com/send?phone=556191542514&text=Isso%20%C3%A9%20um%20teste%20de%20software!");
        driver.findElement(By.cssSelector("a[title='Compartilhe no WhatsApp']")).click();
        driver.findElement(By.linkText("use o WhatsApp Web")).click();
        driver.findElement(By.cssSelector("span[data-icon='send']")).click();
        driver.quit();
    }

    @GetMapping(value="/get-perfil")
    public Perfil getPerfil() throws InterruptedException, IOException {
        System.setProperty("webdriver.chrome.driver", "C:\\selenium-webdriver\\chromedriver.exe");
        String userProfile= "./Google/Chrome/User Data/";
        ChromeOptions options = new ChromeOptions();
        options.addArguments("user-data-dir="+userProfile);
        options.addArguments("--start-maximized");
        options.addArguments("chrome.switches", "--disable-extensions") ;
        WebDriver driver = new ChromeDriver(options);

        Perfil perfil = new Perfil();
        try {
            driver.get("https://web.whatsapp.com/");

            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(ExpectedConditions.or(
                    ExpectedConditions.elementToBeClickable(By.cssSelector("canvas[aria-label='Scan me!']")),
                    ExpectedConditions.elementToBeClickable(By.cssSelector("div[id='side'] div[style*='cursor: pointer;'] img[src^='https://web.whatsapp.com/pp']"))
            ));

            perfil = perfil.getPerfil(driver);

        } catch (Exception e) {

        } finally {
            driver.quit();
        }
        return perfil;
    }

    @GetMapping(value="/set-login")
    public Perfil setLogin() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\selenium-webdriver\\chromedriver.exe");
        String userProfile= "./Google/Chrome/User Data/";
        ChromeOptions options = new ChromeOptions();
        options.addArguments("user-data-dir="+userProfile);
        options.addArguments("--start-maximized");
        options.addArguments("chrome.switches", "--disable-extensions") ;
        WebDriver driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
        Perfil perfil = new Perfil();
        try {
            driver.get("https://web.whatsapp.com/");
            driver.findElement(By.cssSelector("div[id='side'] div[style*='cursor: pointer;'] img[src^='https://web.whatsapp.com/pp']"));
            return perfil.getPerfil(driver);
        } catch (Exception e) {
            return perfil;
        } finally {
            driver.quit();
        }
    }

    @GetMapping(value="/set-logout")
    public void setLogout() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\selenium-webdriver\\chromedriver.exe");
        String userProfile= "./Google/Chrome/User Data/";
        ChromeOptions options = new ChromeOptions();
        options.addArguments("user-data-dir="+userProfile);
        options.addArguments("--start-maximized");
        options.addArguments("chrome.switches", "--disable-extensions") ;
        WebDriver driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(300, TimeUnit.SECONDS);
        Perfil perfil = new Perfil();
        try {
            driver.get("https://web.whatsapp.com/");
            WebElement btnMenu = new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(By.cssSelector("span[data-testid='menu'][data-icon='menu']")));
            btnMenu.click();
            WebElement btnLogout = new WebDriverWait(driver, 30).until(ExpectedConditions.elementToBeClickable(By.cssSelector("div[role='button'][aria-label='Desconectar']")));
            btnLogout.click();
        } catch (Exception e) {
        } finally {
            driver.quit();
        }
    }

    @GetMapping(value="/is-logado")
    public boolean isLogado() {
        System.setProperty("webdriver.chrome.driver", "C:\\selenium-webdriver\\chromedriver.exe");
        String userProfile= "./Google/Chrome/User Data/";
        ChromeOptions options = new ChromeOptions();
        options.addArguments("user-data-dir="+userProfile);
        options.addArguments("--start-maximized");
        options.addArguments("chrome.switches", "--disable-extensions") ;
        WebDriver driver = new ChromeDriver(options);
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);

        driver.get("https://web.whatsapp.com/");
        boolean res;
        if (driver.findElements(By.cssSelector("canvas[aria-label='Scan me!']")).size() > 0) {
            res = false;
        } else {
            res = true;
        }
        driver.quit();
        return res;
    }

}
